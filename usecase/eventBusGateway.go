package usecase

import (
	"gofri/fridge/domain"
)

type EventSender interface {
	SendEvent(Event)
}

type Event interface {
	GetHeader() map[string]string
	GetPayload() string
}

type ProductUpdateEvent struct {
	Product *domain.Product
}

// TODO test me
func (ev *ProductUpdateEvent) GetHeader() map[string]string {
	retMap := make(map[string]string)
	retMap["Type"] = "ProductUpdate"
	retMap["Id"] = ev.Product.Code.Code
	retMap["Name"] = "ProductUpdate" + ev.Product.Name
	return retMap
}

// TOOD test me
func (ev *ProductUpdateEvent) GetPayload() string {
	return ""
}
