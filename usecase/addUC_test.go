package usecase

import (
	"errors"
	"reflect"
	"testing"

	dom "gofri/fridge/domain"
)

func TestWhenProductIsNil_ThenReturnError(t *testing.T) {
	uc, _, _, _ := mock(workingRepo)

	_, err := uc.Add(nil)
	if err.Error() != "product cannot be nil" {
		t.Error("Expected error on nil product")
	}
}

func TestWhenCodeIsNil_ThenReturnError(t *testing.T) {
	uc, _, _, product := mock(workingRepo)
	product.Code = nil

	_, err := uc.Add(product)
	if err.Error() != "code cannot be nil" {
		t.Error("Expected error on nil products code")
	}
}

// TODO should I test nil repo or sender? mby create helper "New" func?

func TestGivenWorkingRepository_WhenAddUCallsSaveCommand_ThenAddReturnsUpdatedProduct(t *testing.T) {
	uc, expectedProd, _, prod := mock(workingRepo)

	result, err := uc.Add(prod)
	if err != nil {
		t.Error(err)
	}

	if result != expectedProd {
		t.Error()
	}
}

func TestGivenNotWorkingRepository_WhenAddUCallsSaveCommand_ThenAddReturnsWithError(t *testing.T) {
	uc, _, _, prod := mock(notWorkingRepo)

	_, err := uc.Add(prod)
	if err == nil {
		t.Error("Expected error to be passed from repository error")
	}
}

func TestGivenWorkingRepository_ThenAddCallsTheEventBus(t *testing.T) {
	expectedProd := getOkProduct()
	repo := &repoOk{nil, expectedProd}
	sender := &senderWorking{}
	expectedEvent := &ProductUpdateEvent{expectedProd}
	prod := getOkProduct()
	uc := &AddUC{repo, sender}

	uc.Add(prod)

	if assertEq(sender.wasCalledWith.GetHeader(), expectedEvent.GetHeader()) == false {
		t.Error("EventBus was not called with product.")
	}
	if sender.wasCalledWith.GetPayload() != expectedEvent.GetPayload() {
		t.Error("EventBus was not called with product.")
	}
}

func assertEq(a, b interface{}) bool {
	return reflect.DeepEqual(a, b)
}

// Functions below are here just to learn how to pass function as argument etc.

type repoOk struct {
	calledWithProduct *dom.Product
	returnedProduct   *dom.Product
}

func (m *repoOk) Save(product *dom.Product) (*dom.Product, error) {
	m.calledWithProduct = product
	return m.returnedProduct, nil
}

func (m *repoOk) Find(code string) (*dom.Product, error) {
	return nil, nil
}

func workingRepo(returnValue *dom.Product) ProductRepository {
	return &repoOk{nil, returnValue}
}

/// repo mocks

type repoNotOk struct {
	calledWithProduct *dom.Product
	returnedProduct   *dom.Product
}

func (m *repoNotOk) Save(product *dom.Product) (*dom.Product, error) {
	m.calledWithProduct = product
	return nil, errors.New("something went wrong")
}

func (m *repoNotOk) Find(code string) (*dom.Product, error) {
	return nil, nil
}

func notWorkingRepo(returnValue *dom.Product) ProductRepository {
	return &repoNotOk{nil, returnValue}
}

func mock(repoF func(*dom.Product) ProductRepository) (uc *AddUC, expectedProd *dom.Product, repo ProductRepository, prod *dom.Product) {
	expectedProd = getOkProduct()
	repo = repoF(expectedProd)
	uc = &AddUC{repo, &senderWorking{}}
	prod = getOkProduct()
	return
}

func getOkProduct() *dom.Product {
	amount := &dom.Amount{Quantity: 1, Unit: &dom.ItemCountUnit{Count: 1}}
	code := &dom.Code{Code: "TEST-1234", Type: "test"}

	return &dom.Product{Amount: amount, Code: code, Name: "TestProduct"}
}

/// sender mocks

type senderWorking struct {
	wasCalledWith Event
}

func (s *senderWorking) SendEvent(event Event) {
	s.wasCalledWith = event
}
