package usecase

import "testing"

func TestProductEvent_getHeader(t *testing.T) {
	prod := getOkProduct()
	out := &ProductUpdateEvent{prod}

	actual := out.GetHeader()

	if actual["Type"] != "ProductUpdate" {
		t.Error("Expected type to be ProductUpdate")
	}
	if actual["Id"] != prod.Code.Code {
		t.Error("Expected Id to be errorcode")
	}
	if actual["Name"] != "ProductUpdate"+prod.Name {
		t.Error("Expected name to be product's name")
	}
}

func TestProductEvent_WhenCodeIsNil(t *testing.T) {
	t.Skip()
}

// func getOkProduct() *domain.Product {
// 	amount := &domain.Amount{Quantity: 1, Unit: &domain.ItemCountUnit{Count: 1}}
// 	code := &domain.JustCode{Code: "TEST-12345"}

// 	return &domain.Product{Amount: amount, Code: code, Name: "TestProduct"}
// }
