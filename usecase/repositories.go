package usecase

import dom "gofri/fridge/domain"

// ProductRepository used by use cases.
type ProductRepository interface {
	Save(*dom.Product) (*dom.Product, error)
	Find(code string) (*dom.Product, error)
}
