package usecase

import dom "gofri/fridge/domain"
import "errors"
import "gofri/fridge/domain"

// AddUC .
type AddUC struct {
	Repository  ProductRepository
	EventSender EventSender
}

type GetUC struct {
	Repository ProductRepository
}

// Add use case implementation.
func (uc *AddUC) Add(product *dom.Product) (*dom.Product, error) {
	if product == nil {
		return nil, errors.New("product cannot be nil")
	}

	if product.Code == nil {
		return nil, errors.New("code cannot be nil")
	}

	updatedProduct, repoErr := uc.Repository.Save(product)
	if repoErr != nil {
		return nil, repoErr
	}

	sendUpdateEvent(uc, updatedProduct)

	return updatedProduct, nil
}

func sendUpdateEvent(uc *AddUC, product *dom.Product) {
	updateEvent := &ProductUpdateEvent{product}
	uc.EventSender.SendEvent(updateEvent)
}

func (uc *GetUC) Get(code string) (*domain.Product, error) {
	return uc.Repository.Find(code)
}
