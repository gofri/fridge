package infrastructure

import "gofri/fridge/usecase"
import "log"

type loggingEventSender struct {
}

func (ev *loggingEventSender) SendEvent(event usecase.Event) {
	log.Printf("Sending Event: %T\n%+v", event, event)
}

func NewLoggingEventSender() usecase.EventSender {
	return &loggingEventSender{}
}
