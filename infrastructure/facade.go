package infrastructure

import (
	"gofri/fridge/repository"
	"gofri/fridge/usecase"
)

var repoInstance repository.ProductRepository

func GetAddProductUC() *usecase.AddUC {
	return &usecase.AddUC{
		EventSender: NewLoggingEventSender(),
		Repository:  getRepository(),
	}
}

func GetGetUC() *usecase.GetUC {
	return &usecase.GetUC{
		Repository: getRepository(),
	}
}

// todo: thread-safety..
func getRepository() repository.ProductRepository {
	if repoInstance == nil {
		repoInstance = repository.NewInMemRepository()
	}
	return repoInstance
}
