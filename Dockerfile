FROM scratch

EXPOSE 8080

ADD fridge-linux /srv/fridge

ENTRYPOINT ["/srv/fridge"]