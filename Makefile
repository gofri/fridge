# .PHONY compile test

all: deps test run

deps:
	dep ensure

test:
	go test ./...

verify:
	go fmt ./...
	go vet ./...
	go test ./... -race

compile: test
	go build

compile-linux:
	env GOOS=linux go build -o fridge-linux

run: compile
	./fridge

dbuild: compile-linux
	docker build -t registry.gitlab.com/gofri/fridge .

drun: dbuild
	docker run --rm -p 8080:8080 --memory=20m registry.gitlab.com/gofri/fridge

dpush: dbuild
	docker push registry.gitlab.com/gofri/fridge

monitoring-build:
	docker build -t registry.gitlab.com/gofri/fridge/monitoring monitoring

monitoring-run: monitoring-build
	docker run --rm -p 19999:19999 --memory=400m registry.gitlab.com/gofri/fridge/monitoring