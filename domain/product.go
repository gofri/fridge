package domain

// Product entity.
type Product struct {
	Name   Name
	Code   *Code
	Amount *Amount
}

// Name describes Product's name.
type Name = string

// Code describes Product's code.
type Code struct {
	Code string
	Type string
}

// Amount specifies amount of the Product using Unit interface.
type Amount struct {
	Quantity float64
	Unit     Unit
}
