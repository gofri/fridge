package domain

// Unit interface allows to use different dimentions,quantities,.. to describe Product's amount.
type Unit interface {
	GetName() string
	GetAmount() float64
}

// ItemCountUnit .
type ItemCountUnit struct {
	Count int64
}

func (i *ItemCountUnit) GetName() string {
	return "ItemCount"
}

func (i *ItemCountUnit) GetAmount() float64 {
	return float64(i.Count)
}

// WeightUnit .
type WeightUnit struct {
	Unit   string
	Weight float64
}

func (w *WeightUnit) GetName() string {
	return "Weight " + w.Unit
}

func (w *WeightUnit) GetAmount() float64 {
	return w.Weight
}
