package gateway

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gofri/fridge/domain"
	"gofri/fridge/infrastructure"
	"gofri/fridge/monitoring"
	"io"
	"log"
	"net/http"
)

func handleProductAdd(w http.ResponseWriter, req *http.Request) {
	log.Printf("REQ %s /product\n", req.Method)
	switch req.Method {
	case http.MethodPost:
		addProduct(w, req.Body)
	}
}
func addProduct(w http.ResponseWriter, body io.ReadCloser) {
	product, err := readProductFromBody(body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		monitoring.Get().BadRequests.Add(1)
		return
	}

	uc := infrastructure.GetAddProductUC()
	_, err = uc.Add(product)

	// todo: what about error classes?
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
	} else {
		w.WriteHeader(http.StatusAccepted)
		monitoring.Get().ProductUpdate.Add(1)
	}
}

func readProductFromBody(body io.ReadCloser) (*domain.Product, error) {
	prod := &domain.Product{}
	data, err := readBody(body)
	if err == nil {
		err = json.Unmarshal(data, prod)
	}
	return prod, err
}

func readBody(body io.ReadCloser) ([]byte, error) {
	buf := bytes.Buffer{}
	n, err := buf.ReadFrom(body)
	log.Printf("Read %d bytes\n", n)
	return buf.Bytes(), err
}
