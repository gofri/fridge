package gateway

import (
	_ "expvar"
	"gofri/fridge/monitoring"
	"net/http"

	"github.com/gorilla/mux"
)

func StartRest() {
	r := mux.NewRouter()
	r.HandleFunc("/product", handleProductAdd)
	r.HandleFunc("/product/{code}", handleProductGet)
	r.Handle("/debug/vars", http.DefaultServeMux)
	monitoring.Get()

	http.ListenAndServe(":8080", r)
}
