package gateway

import (
	"encoding/json"
	"fmt"
	"gofri/fridge/domain"
	"gofri/fridge/infrastructure"
	"gofri/fridge/monitoring"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func handleProductGet(w http.ResponseWriter, req *http.Request) {
	log.Printf("REQ %s %s\n", req.Method, req.URL.Path)
	code := mux.Vars(req)["code"]

	if len(code) != 0 {
		product, _ := infrastructure.GetGetUC().Get(code)
		writeProduct(product, w)
	}
}

func writeProduct(prod *domain.Product, w http.ResponseWriter) {
	if prod == nil {
		w.WriteHeader(http.StatusNotFound)
		monitoring.Get().BadRequests.Add(1)
	} else if data, err := json.Marshal(prod); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, err.Error())
	} else {
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		monitoring.Get().ProductGet.Add(1)
	}
}
