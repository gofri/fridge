package main

import (
	"gofri/fridge/gateway"
)

func main() {
	gateway.StartRest()
}
