package monitoring

import (
	"expvar"
	"sync"
)

type productUCMonitor struct {
	ProductUpdate *expvar.Int
	ProductGet    *expvar.Int
	BadRequests   *expvar.Int
}

var once sync.Once
var monitor *productUCMonitor

func Get() *productUCMonitor {
	once.Do(func() {
		monitor = new()
		initVMMonitoring()
	})

	return monitor
}

func new() *productUCMonitor {
	return &productUCMonitor{
		ProductUpdate: newVar("product-updates"),
		ProductGet:    newVar("product-gets"),
		BadRequests:   newVar("bad-requests"),
	}
}

func newVar(name string) *expvar.Int {
	return newVarWithDefault(name, 0)
}

func newVarWithDefault(name string, def int64) *expvar.Int {
	evar := expvar.NewInt(name)
	evar.Set(0)
	return evar
}
