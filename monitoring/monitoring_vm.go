package monitoring

import (
	"expvar"
	"runtime"
	"time"
)

func initVMMonitoring() {
	tick := time.NewTicker(1 * time.Second)
	routs := expvar.NewInt("runtime-goroutines")

	go func() {
		for {
			select {
			case <-tick.C:
				routs.Set(int64(runtime.NumGoroutine()))
			}
		}
	}()
}
