package repository

import (
	"errors"
	"gofri/fridge/domain"
	"sync/atomic"
)

type ProductRepository interface {
	Save(*domain.Product) (*domain.Product, error)
	Find(code string) (*domain.Product, error)
}

type inMemoryProductRepository struct {
	cache map[string]*domain.Product
	id    uint64
}

func (r *inMemoryProductRepository) Save(product *domain.Product) (*domain.Product, error) {
	r.cache[product.Code.Code] = product
	atomic.AddUint64(&r.id, 1)

	return product, nil
}

func (r *inMemoryProductRepository) Find(code string) (*domain.Product, error) {
	if len(code) == 0 {
		return nil, errors.New("code cannot be empty")
	}

	return r.cache[code], nil
}

func NewInMemRepository() ProductRepository {
	return &inMemoryProductRepository{
		cache: make(map[string]*domain.Product),
	}
}
